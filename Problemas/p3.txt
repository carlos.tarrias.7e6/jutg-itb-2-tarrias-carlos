Problema 3.Calcula l’àrea
===========Descripció=====
Una web d'habitatges de lloguer ens ha proposat una ampliació. Volen mostrar l'àrea de les
habitacions per llogar. Fes un programa que ens ajudi a calcular les dimensions d'una habitació.
Llegeix l'amplada i la llargada en metres (enters) i mostra'n l'àrea.
===========Entrada========
Per l’entrada sempre rebreu dos nombres enters que representen amplada i llargada.
===========Sortida========
Per la sortida sempre haureu d’imprimir un altre enter amb l’àrea de l’habitació que heu rebut.