# Proyecto Troncal M3 : JutgITB (Version 2.0)
## Autores
- [Carlos Tarrias Diaz](https://gitlab.com/carlos.tarrias.7e6)

## Índice
- [Objetivo](#objetivo)
- [Diagrama de Clases](#diagrama de clases)
- [Descripción](#descripción)
- [Funcionalidades modo Alumno](#Funcionalidades modo Alumno)
- [Funcionalidades modo Profesor](#Funcionalidades modo Profesor)
- [Persistencia de datos](#Persistencia de datos)

## Objetivo
Realizar un juez en línea de problemas de programación, inspirado en otros jueces como [Jutge](https://jutge.org/) o [Joel](https://jo-el.es/contest/testcodejam23). El programa permite entrar en modalidad usuario para gestionar los problemas del sistema, o en modo alumno para poder resolver los problemas.
## Diagrama de clases
El siguiente diagrama UML representa la estructura de clases del Juez ITB de forma simplificada.

![Diagrama Clases UML](./JuezITB_Diagrama_Clases.drawio.png)


## Descripción
Al iniciar el programa, al usuario se le indica si quiere entrar en modo alumno o en modo profesor:
    
   - Modo Alumno:
     - Se le muestra el menú con las siguientes funcionalidades:
       1. Seguir itinerario de aprendizaje.
       2. Resolver problema de la lista.
       3. Consultar historial de problemas.
       4. Consultar ayuda del Juez ITB.
     - Cuando se sale del menú se realiza la operación de guardado de datos del alumno. 
   - Modo Profesor:
     - Se le muestra el menú con las siguientes funcionalidades:
       1. Crear un nuevo problema.
       2. Crear informe del usuario.
     - Cuando se seleccione una de estas opciones el sistema pedirá la contraseña al usuario para acceder a la funcionalidad escogida (solo una vez).
        > La contraseña del modo profesor es **qwe** 
    
   > Durante la ejecución del juez, el usuario solo puede estar en modo alumno o en modo profesor 
   (no se puede cambiar de modo en medio de la ejecución).
   > Cuando en ambos casos se decide salir del menú correspondiente, el juez ITB finaliza su ejecución.
## Funcionalidades modo Alumno

- **Seguir itinerario de aprendizaje**
    - El sistema muestra los títulos de los problemas hasta llegar al primero no resuelto, entonces se le pide al usuario si lo quiere resolver:
      - Si indica que no, el sistema muestra el siguiente problema no resuelto.
      - Si indica que sí, se le muestra el detalle del problema y se le indica que introduzca la respuesta (la respuesta en todos los problemas es siempre un único valor)
        al caso de prueba privado. Si el usuario indica una respuesta incorrecta, puede reintentar el problema indefinidamente hasta resolverlo o parar de intentarlo.
      - Independientemente de si se ha resuelto o no el problema, se muestra el próximo problema no resuelto y se aplica todo el proceso explicado
      desde el principio.
    - Cuando el usuario llega al final de la lista puede decidir verla otra vez o volver al menú.
- **Resolver problema de la lista**:
    - Se muestra una lista de los problemas del usuario en el siguiente orden:
      - Número de problema.
      - Título del problema.
      - Sección a la que pertenece el problema.
      - Indicación de si está o no resuelto.
      - Cruces ('X') seguidas en función del número de intentos del problema. Si no hay intentos no aparece nada.
    - Después de mostrar la lista se le pide al usuario el número del problema que 
    quiere resolver:
      - Si el número introducido no existe, vuelve al menú.
      - Si el problema indicado ya está resuelto, vuelve al menú.
      - Si no pasa lo anterior, el sistema muestra el detalle del problema (enunciado + juego de pruebas) y le pide 
      al usuario la respuesta al caso de prueba privado.
      Una vez resuelto (o el usuario no quiera reintentar más el problema) volvemos al menú.
- **Consultar historial de problemas**
    - Muestra la misma lista que en la segunda opción sin más añadido.
- **Consultar ayuda del Juez ITB**
    - Muestra un texto explicando brevemente que realiza cada función del menú de usuario.
## Funcionalidades modo Profesor

- **Crear un nuevo problema**
  - Para crear el problema se le pide el usuario que introduzca lo siguiente:
    - El título del problema.
    - El enunciado del problema.
    - El número de sección del problema.
    - El número de casos de prueba que contendrá (mínimo 2).
      - Para cada caso de prueba se le pide al usuario:
        - Los inputs separados por espacios.
        - El output del problema (suponemos que siempre hay un solo output)
  - Una vez introducidos los datos se realiza la actualización de los ficheros del sistema para que el 
  alumno pueda ver el/los nuevo/s problema/s en la próxima ejecución del juez.
- **Crear informe del usuario**
  - Se muestra por pantalla lo siguiente:
    - El número total de problemas resueltos.
    - Por cada problema resuelto se muestra su nota. La fórmula es la siguiente:
      - `NOTA_PROBLEMA = numIntentos > 5 ? 0 : 10 - (1*numIntentos)`
      - Internamente empezamos a descontar puntuación a partir del segundo intento.
    - La nota final ponderada en función de los problemas resueltos. La fórmula es la siguiente:
      - `NOTA_FINAL =  sumatorio(NOTA_PROBLEMA*ponderacion)`
      - Tener en cuenta que `ponderacion = 1/numProblemasResueltos`.

## Persistencia de datos
Para implementar la persistencia de datos hemos utilizado las carpetas y archivos siguientes:
 - Carpeta __Problemas__ : Contiene los datos de cada problema del juez ITB.
   > El nombre de archivo de cada problema es __p{id}__.json . 
   La id va de 1 hasta el número de problemas del sistema.
   - Para cada problema guardamos:
     - Id
     - Titulo
     - Si está resuelto o no
     - Número de intentos
     - Número de sección (para clasificarlo más adelante)
     - Lista de respuestas introducidas (realmente no sería estrictamente necesario especificarlo, ya que lo controlamos en el archivo del registro del alumno).
     - Enunciado
     - Lista de pruebas públicas
     - Lista de pruebas privadas (siempre hay 1 elemento para simplificar).
 - Archivo __registroAlumno.json__ : Archivo JSON que contiene los datos de los problemas del alumno. Indicando, para cada problema: 
   - Id
   - Si se ha resuelto o no 
   - El número de intentos 
   - Lista de respuestas introducidas 
 - Archivo __numProblemas.txt__ : Contiene el número de problemas del sistema.
   - Se actualiza cada vez que el profesor crea un nuevo problema.  

Al realizar las operaciones de lectura y escritura (deserialización y serialización) de los archivos en formato **.json**
se ha utilizado la librería [GSON](https://github.com/google/gson).

> Para simplificar la implementación, suponemos que estos archivos y carpetas ya existen en el sistema. Por lo tanto, el sistema no se encarga de crearlos,
y no hace falta que compruebe si estos existen o no a la hora de realizar operaciones de lectura y escritura.

