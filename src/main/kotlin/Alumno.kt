import com.google.gson.Gson
import java.io.File
import kotlin.system.exitProcess

/**
 * Representa a un usuario de tipo alumno.
 * Funcionalidades del alumno.
 * 1-> Seguir con itinerario de problemas (funcionalidad de la versión 1 del juez ITB).
 * 2-> Seleccionar el problema a resolver de la lista.
 * 3-> Mostrar historial de problemas con sus parámetros.
 * 4-> Mostrar ayuda del sistema.
 * @author Carlos Tarrias Diaz
 * @version 1.0.0
 * @see IUsuario
 * @see Alumno
 * @see ListaProblemas
 * @see UI
 */
class Alumno(override var nombre: String): IUsuario {

  //inicialización de atributos.
  var id = nombre + "01" //No se usa, en caso de tener varios usuarios es interesante guardar un id.

  //Inicializamos la lista de problemas deserializando los problemas de la carpeta "Problemas"
  var listaProblema: ListaProblemas = ListaProblemas("./Problemas/")

  /**
   * Permite activar la funcionalidad concreta en función de la opción que indique el usuario.
   * Función de la interfaz IUsuario.
   * @param opcion : String - Opción que introduce el alumno.
   * @return Boolean - Indica si el alumno quiere salir del sistema o no.
   */
  override fun activarFuncionalidad(opcion:String):Boolean {
    var quiereSalir = false
    when(opcion) {
      "1" -> this.listaProblema.mostrarEnunciadoProblemas()
      "2" -> this.mostrarListaDeProblemas()
      "3" -> println(this.listaProblema)
      "4" ->  UI.mostrarAyuda()
      "5" -> {
        quiereSalir = true
        println("Saliendo del modo usuario")
        //Guardamos datos para asegurar un sistema persistente.
        this.guardarRegistro()
      }
      else -> exitProcess(-1) //Para simplificar, salimos del programa si el usuario no introduce la opción correcta.
    }
    return quiereSalir
  }

  /**
   * Permite guardar los datos de los problemas del alumno con los parámetros actualizados. Los datos del alumno están en "registroAlumno.json"
   * El alumno contiene para cada problema lo siguiente -> (id, numIntentos, resuelto, listaIntentos).
   */
  private fun guardarRegistro() {
    var registroAGuardar = this.listaProblema.registroAlumno
    // Mientras el usuario va resolviendo problemas éstos ven modificados sus atributos. Estos cambios están reflejados en la lista de
    // problemas pero hace falta pasar estos cambios al objeto registro
    // para que a la hora de guardar los datos el archivo 'registroAlumno.json' tenga los datos más recientes para futuros accesos del alumno.
    for (i in this.listaProblema.problemas.indices) {
      registroAGuardar.cambiarAtributosProblema(
        this.listaProblema.problemas[i].id,
        this.listaProblema.problemas[i].numeroIntentos.toString(),
        this.listaProblema.problemas[i].resuelto.toString(),
        this.listaProblema.problemas[i].listaIntentos,
        i
      )
    }

    //Una vez tenemos el registro actualizado sobreescribimos los datos del alumno mediante la librería GSON.
    val gson = Gson()
    val json = gson.toJson(registroAGuardar)
    var file = File("./registroAlumno.json")
    file.writeText("")
    file.writeText(json)
  }

  /**
   * Muestra la lista de problemas del sistema e indica al usuario cuál quiere resolver.
   * Opción 2 del menú del alumno.
   */
  private fun mostrarListaDeProblemas() {
    this.listaProblema.mostrarListaTitulosProblemas()
    println("¿Qué problema quieres resolver?")
    val numeroProblemaEscogido: Int = readln().toInt()
    if (numeroProblemaEscogido !in (1..SistemaJutgeITB.getNumProblemes()))  { //Miramos si el id del problema existe.
      println("El problema escogido no existe...")
      return //Volvemos al menú del alumno.
    }
    val problemaEscogido: Problema = this.listaProblema.problemas[numeroProblemaEscogido-1]
    if (problemaEscogido.resuelto) { //Miramos si el problema está resuelto.
      println("El problema escogido ya está resuelto...")
    }
    else { //Si el problema no está resuelto y el id introducido existe.
      println(problemaEscogido)
      problemaEscogido.mostraCasosPublicos()
      resolverProblema(problemaEscogido)
    }
  }

}