/**
 * Representa un caso de prueba que forma parte de un problema del Juez ITB. Contiene una lista de inputs y outputs y puede ser público o privado.
 * La clase GSON para (serializar JSON) necesita que las clases a serializar tengan un constructor con atributos.
 * Por ello he dejado el parámetro 'ruta' sin modificar en el constructor y así evitar errores.
 * @author Carlos Tarrias Diaz
 * @version 2.0.0
 * @see TipoCaso
 */
class CasoPrueba(ruta:String) {

  //Inicialización de atributos.
  var inputs = mutableListOf<String>()
  var outputs = mutableListOf<String>()
  var tipo : String = ""

  //Indicamos Transient para que a la hora de serializar un problema, este campo se ignore.
  @Transient
  var tipoCaso = TipoCaso.DEFAULT

  /**
   * Asigna el tipo de caso en función del codigo ('O' ó 'V') que tiene asignado.
   */
  fun setTipo() {

    val caracterATipo = mapOf(
      "O" to TipoCaso.PRIVADO,
      "V" to TipoCaso.PUBLICO,
    )
    this.tipoCaso = caracterATipo[this.tipo]!!
  }

  /**
   * Sobrecarga de la representación en String del objeto CasosPrueba.
   * @return String: Representación del caso de prueba en formato texto.
   */
  override fun toString(): String {
    val caso = """
      | ==============CASO DE PRUEBA ${this.tipoCaso}============ 
      | =====>ENTRADAS 
      | ${this.inputs} 
      | =====>SALIDAS 
      | ${this.outputs} 
      """.trimMargin()
    return "${caso}\n"
  }
}