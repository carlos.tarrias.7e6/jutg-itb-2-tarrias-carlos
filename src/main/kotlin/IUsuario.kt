/**
 * Representa el contrato que deben seguir los diferentes tipos de usuarios (Alumno o Profesor)
 * @author Carlos Tarrias Diaz
 * @version 1.0.0
 * @see Alumno
 * @see Profesor
 */
interface IUsuario {
 var nombre: String
 fun activarFuncionalidad(opcion:String): Boolean
}