import com.google.gson.Gson
import java.io.File

/**
 * Representa el informe de notas del alumno.
 * @author Carlos Tarrias Diaz
 * @version 1.0.0
 * @see Profesor
 * @see Alumno
 * @see UI
 */
class InformeAlumno {

  //Inicialización de atributos
  var notaFinal = 0.0
  private var registroAlumno: RegistroAlumno = RegistroAlumno("")
  var problemasResueltos : List<RegistroAlumno.Campos> = listOf()
  var numProblemasResueltos : Int = 0
  var ponderacion: Double = 0.0

  // Vamos calculando las notas en función de los datos de los problemas del alumno que leemos del archivo 'registroAlumno.json'.
  init {
    //Fórmula de puntuación individual de problemas resueltos:
        //SI numIntentos <= 5 NOTA_P=10-(1*numIntentos) SINO NOTA = 0
    //Fórmula ponderada para cálculo total de la nota
      // ponderacion <- 1/numProblemasResueltos
      // NOTA FINAL = sumatorio(NOTA_P1*ponderacion...NOTA_PN*ponderacion)
    this.registroAlumno = getRegistroAlumno()
    this.problemasResueltos = registroAlumno.problemas.filter {it.resuelto.toBoolean()}
    this.numProblemasResueltos = this.problemasResueltos.size
    this.ponderacion = 1.0 / this.numProblemasResueltos.toDouble()

    var notaProblema : Int
    for (problema in this.problemasResueltos) {
      notaProblema = if (problema.numIntentos.toInt()-1 <= 5) 10 - (1*problema.numIntentos.toInt()-1) else 0
      this.notaFinal += notaProblema.toDouble() * ponderacion
    }
  }

  /**
   * Permite deserializar los datos del alumno para inicializar el informe.
   */
  private fun getRegistroAlumno(): RegistroAlumno {
    val gson = Gson()
    val file = File("./registroAlumno.json")
    val jsonRegistroNoActualizado = file.readText()
    return gson.fromJson(jsonRegistroNoActualizado, RegistroAlumno::class.java)
  }
}