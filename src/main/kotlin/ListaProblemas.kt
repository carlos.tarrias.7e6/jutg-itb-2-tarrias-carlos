import com.google.gson.Gson
import java.io.File

/**
 * Representa la lista de problemas del juez asignada al alumno.
 * @author Carlos Tarrias Diaz
 * @version 1.0.0
 * @see Alumno
 * @see Problema
 * @see RegistroAlumno
 */
class ListaProblemas(ruta: String) {

  //Inicialización de atributos.
  var problemas = mutableListOf<Problema>()
  private val numProblemas = SistemaJutgeITB.getNumProblemes()
  var registroAlumno : RegistroAlumno = RegistroAlumno("")


  init {

    //Carga de datos de los problemas. Tiene dos fases.

    //1. Cargamos datos generales de cada problema mediante la clase auxiliar 'Record'.
    var problema: Problema
    for (i in 1..this.numProblemas) {
      problema = Record("${ruta}p${i}.json").getProblemaDeJSON()
      problema.setAttributtes()
      this.problemas.add(problema)
    }

    //2.1. Leemos los datos de los problemas del usuario para que el sistema tenga los datos más recientes.
    val gson = Gson()
    val file = File("./registroAlumno.json")
    val json = file.readText()
    this.registroAlumno = gson.fromJson(json, RegistroAlumno::class.java)

    //2.2 Cargamos los datos correspondientes del usuario en la estructura.
    for (i in 0 until this.problemas.size) {
      this.problemas[i].resuelto = this.registroAlumno.getResuelto(i).toBoolean()
      this.problemas[i].numeroIntentos = this.registroAlumno.getNumIntentos(i).toInt()
      this.problemas[i].listaIntentos = this.registroAlumno.getListaIntentos(i)
    }

  }

  /**
   * Para mostrar los problemas con su información correspondiente.
   */
  fun mostrarListaTitulosProblemas() {
    println(this)
  }

  /**
   * Sirve para mostrar cierta información del problema en función de si éste está resuelto o no.
   * y para permitir la opción de resolverlo (si no está resuelto).
   */
  fun mostrarEnunciadoProblemas() {
    var opcionUsuarioMenu : String
    do {
      for (problema in this.problemas) {
        //Si el problema ya está resuelto o hemos indicado que no queremos resolverlo, pasamos al siguiente problema de la lista.
        println("PROBLEMA ${problema.id} - ${problema.titulo}")
        if (!decidirProblema(problema)) continue //función externa de utils.kt
        println(problema)
        //Mostramos enunciados del problema y sus casos públicos.
        problema.mostraCasosPublicos()
        resolverProblema(problema) //función externa de utils.kt
      }
      println("¿Quieres ver los enunciados otra vez? (Sí:S/s , No:N/n)")
      opcionUsuarioMenu = readln().trim()
    }while (opcionUsuarioMenu !in "nN") //Mostramos los enunciados en bucle hasta que el usuario indique lo contrario al final.

  }

  /**
   * Sobreecarga que permite mostrar un historial de problemas del sistema.
   */
  override fun toString(): String {
    var mensaje = ""
    for (problema in this.problemas) {
      val resueltoString = if (!problema.resuelto) "\u001b[33m" + "NO RESUELTO" + "\u001b[0m" else "\u001b[32m" + "RESUELTO" + "\u001b[0m"
      if (!problema.resuelto) "\u001b[33m" + "NO RESUELTO" + "\u001b[0m" else "\u001b[32m" + "RESUELTO" + "\u001b[0m"
      var intentosString = ""
      val seccionString: String = "\u001b[36m" + problema.seccion + "\u001b[0m"
      for (i in 1..problema.numeroIntentos) intentosString += "X"
      mensaje += "Problema ${problema.id}. ${problema.titulo} *${seccionString}* |${resueltoString}| $intentosString \n"
    }

    return mensaje
  }
}