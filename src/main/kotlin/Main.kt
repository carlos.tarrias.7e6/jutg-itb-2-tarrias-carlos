/**
 * Punto de entrada del programa Juez ITB.
 * @author Carlos Tarrias Diaz
 * @version 2.0.0.
 */
fun main() {
  SistemaJutgeITB.iniciar()
}

