/**
 * Representa cada uno de los problemas de programación del juez ITB que se le enseñarán al usuario.
 * Tipo -> data : parcialmente sólo sirve para almacenar datos que se van a serializar.
 * Indicamos @Transient para que a la hora de serializar un problema, este campo se ignore.
 * @author Carlos Tarrias Diaz
 * @version 2.0.0
 * @see CasoPrueba
 */
data class Problema(@Transient var nombreFichero:String, @Transient var ficheroCasos:String) {
   var id = ""
   var titulo = ""
   var resuelto: Boolean = false
   var numeroIntentos: Int = 0
   var numeroSeccion = "0"
   var listaIntentos : MutableList<String> = mutableListOf()
   var enunciado = ""
   var juegosPruebaPublicos = mutableListOf<CasoPrueba>()
   var juegosPruebaPrivados = mutableListOf<CasoPrueba>()

   @Transient
   var seccion : Secciones = Secciones.DEFAULT
  /**
   * Instrucciones de inicialización del objeto.
   */
  fun setAttributtes() {
    //1.Establecemos seccion a la que pertenece el problema.
    val numeroASeccion = mapOf(
      "1" to Secciones.TDADES,
      "2" to Secciones.CONDICIONAL,
      "3" to Secciones.BUCLE,
      "4" to Secciones.LLISTA
    )
    this.seccion = numeroASeccion[this.numeroSeccion]!!
    this.juegosPruebaPublicos.forEach { it.setTipo() }
  }

  /**
   * Añade la respuesta del usuario en un intento concreto a la lista de respuestas del problema.
   * @param intento String : Respuesta del usuario.
   */
  fun addIntento(intento: String) {
    this.listaIntentos.add(intento)
  }

  /**
   * Incrementa el número de veces que ha intentado responder el usuario al problema.
   */
  fun incrementarIntentos() = this.numeroIntentos++

  /**
   * Muestra la lista de casos públicos que contiene el problema (cada caso con sus entradas y salida).
   */
  fun mostraCasosPublicos() {
    this.juegosPruebaPublicos.forEach { println(it) }
  }

  /**
   * Devuelve la respuesta al problema
   * De momento suponemos que sólo existe una única respuesta y que sólo hay un caso privado a resolver.
   * @return String : Representa la respuesta al problema.
   */
  fun getRespuesta():String = this.juegosPruebaPrivados[0].outputs[0]

  /**
   * Muestra la lista de casos privados (de momento sólo uno) que contiene el problema.
   * De cada caso privado sólo se muestran sus entradas/inputs.
   */
  fun mostraCasosPrivados() {
    juegosPruebaPrivados.forEach {
      val caso = """
      | ==============CASO DE PRUEBA PRIVADO============ 
      | =====>ENTRADAS 
      | ${it.inputs}
      """.trimMargin()
      println(caso)
    }
  }

  /**
   * Sobrecarga de la representación en String del objeto Problema.
   * @return String: Representación del Problema en formato texto.
   */
  override fun toString(): String {
    return "PROBLEMA ${this.id} ${this.titulo} \n${this.enunciado}\nINTENTOS REALIZADOS : ${this.numeroIntentos}\nRESPUESTAS ANTERIORES : ${this.listaIntentos}\n"
  }
}