import com.google.gson.Gson
import java.io.File
import kotlin.system.exitProcess

/**
 * Representa a un usuario de tipo profesor.
 * Funcionalidades del profesor.
 * 1-> Añadir nuevo problema al sistema.
 * 2-> Generar informe de notas del alumno.
 * @author Carlos Tarrias Diaz
 * @version 1.0.0
 * @see Secciones
 * @see IUsuario
 */
class Profesor(override var nombre: String) :IUsuario {

  //inicialización de atributos.
  private var contrasenya = "qwe"
  private var tieneAcceso = false

  /**
   * Permite activar la funcionalidad concreta en función de la opción que indique el usuario.
   * Función de la interfaz IUsuario.
   * @param opcion : String - Opción que introduce el profesor.
   * @return Boolean - Indica si el profesor quiere salir del sistema o no.
   */
  override fun activarFuncionalidad(opcion:String):Boolean {
    if (!this.tieneAcceso && opcion.toInt() in 1..2) { //Comprobamos si el profesor ya ha introducido la contraseña correcta o no.
      if (!comprobarContrasenya()) { //Comprobamos si la contraseña introducida es correcta.
        println("La contraseña introducida no es válida...")
        return true
      }
      else { //Una vez que se ha verificado, ya tiene acceso a las funciones del menú.
        println("Contraseña válida.")
        this.tieneAcceso = true
      }
    }
    return seleccionarFuncion(opcion)
  }

  /**
   * Permite activar la funcionalidad concreta en función de la opción que indique el profesor.
   * @param opcion : String - Opción que introduce el profesor.
   * @return Boolean - Indica si el profesor quiere salir del sistema o no.
   */
  private fun seleccionarFuncion(opcion:String): Boolean {
    var quiereSalir = false
    when(opcion) {
      "1" -> generarNuevoProblema()
      "2" -> generarInformeAlumno()
      "3" -> {
        quiereSalir = true
        println("Saliendo del modo profesor")
      }
      else -> exitProcess(-1) //Para simplificar, salimos del programa si el usuario no introduce la opción correcta.
    }
    return quiereSalir
  }

  /**
   * Permite crear un nuevo problema en función de los datos que introduzca el profesor.
   * @param problema : Problema - Representa el problema que se añadira al sistema (paso por referencia).
   */
  private fun crearProblema(problema:Problema) {

    //Correspondencia del numero de seccion con el tipo enumerado concreto.
    val numeroASeccion = mapOf(
      "1" to Secciones.TDADES,
      "2" to Secciones.CONDICIONAL,
      "3" to Secciones.BUCLE,
      "4" to Secciones.LLISTA
    )

    //Introducir atributos de problema.
    problema.id = SistemaJutgeITB.getNumProblemes().toString()
    println("Escribe el título del nuevo problema.")
    problema.titulo = readln().trim()
    println("Escribe el enunciado del nuevo problema.")
    problema.enunciado = readln().trim()
    println("¿A qué sección corresponde el problema? (1:TDATA,2:CONDICIONAL,3:BUCLE,4.LLISTA")
    val numeroSeccion = readln().trim()
    problema.numeroSeccion = numeroSeccion
    problema.seccion = numeroASeccion[numeroSeccion]!!
    println("¿Cuántos casos de prueba quieres generar (por defecto 1 público y 1 privado) ?")
    println("NOTA: Ten en cuenta que el último caso introducido será el caso privado del problema.")

    //Introducir juego de pruebas
    val juegosPrueba = this.crearJuegoDePruebas(readln().trim().toInt())

    //Imponemos que el último caso de prueba de la lista creada corresponde al caso de pruebas privado y el resto son públicos.
    problema.juegosPruebaPublicos = juegosPrueba.subList(0, juegosPrueba.lastIndex)
    problema.juegosPruebaPrivados = juegosPrueba.subList(juegosPrueba.lastIndex, juegosPrueba.lastIndex + 1)
  }

  /**
   * Permite crear el juego de pruebas del nuevo problema.
   * @param input : Int - Representa el número de casos de prueba que introducirá el profesor.
   */
  private fun crearJuegoDePruebas(input:Int):MutableList<CasoPrueba> {
    var numeroCasos = input
    if (numeroCasos < 2) numeroCasos = 2 //Para que haya coherencia con la estructura de un problema necesitamos mínimo 1 caso publico y 1 caso provado.
    var inputs: MutableList<String>
    var outputs: MutableList<String>
    var casoPrueba: CasoPrueba
    val juegoPruebas = mutableListOf<CasoPrueba>()

    //Tenemos que especificar los datos de los inputs como el de los outputs para cada caso de prueba.
    //Para simplificar imponemos que un caso de prueba puede tener varios inputs pero sólo un único output.
    for (i in 1..numeroCasos) {
      casoPrueba = CasoPrueba("")
      println("Escribe los inputs del caso de prueba $i separados por espacios, por favor:")
      inputs = readln().split(" ").toMutableList()
      println("Escribe el output del caso de prueba (un sólo valor) $i , por favor:")
      outputs = readln().split(" ").toMutableList()
      println("Guardando caso de prueba $i...")
      casoPrueba.inputs = inputs
      casoPrueba.outputs = outputs
      casoPrueba.tipo = if (i==numeroCasos) "O" else "V"
      casoPrueba.tipoCaso = if (i==numeroCasos) TipoCaso.PRIVADO else TipoCaso.PUBLICO
      juegoPruebas.add(casoPrueba)
    }
    return juegoPruebas
  }

  /**
   * Permite serializar el nuevo problema para añadirlo a la lista de problemas en la carpeta 'Problemas'.
   * @param problema : Problema - Problemas con los datos introducidos por el profesor.
   */
  private fun anyadirProblemaAlSistema(problema: Problema) {
    val gson = Gson()
    val jsonProblema = gson.toJson(problema)
    // Añadimos un nuevo problema en formato JSON al directorio 'Problemas' con identificador igual al número
    // de problemas actual (contando el nuevo).
    val file = File("./Problemas/p${SistemaJutgeITB.getNumProblemes()}.json")
    file.createNewFile()
    file.writeText(jsonProblema)
  }

  /**
   * Permite crear un problema mediante los datos introducidos por terminal y añadirlo a la lista de problemas y
   * a los datos del alumno.
   */
  private fun generarNuevoProblema() {
    // Hay que incrementar el numero de problemas en el fichero 'registroAlumno.json' para poder tener acceso a los nuevos problemas
    // una vez entremos al juez como alumno.
    SistemaJutgeITB.incrementarNumeroProblemas()

    var problemaNuevo = Problema("","")
    crearProblema(problemaNuevo)
    anyadirProblemaAlSistema(problemaNuevo)

    println("Añadiendo problema al sistema.")

    //Añadimos datos del problema al registro del alumno mediante el objeto RegistroAlumno.
    //Para ello añadimos deserializamos el registro para poder añadir el problema creado a la lista de problemas del registro leído (el antiguo)
    //y después serializamos el objeto (el nuevo) para poder tener un archivo de datos actualizado.
    val gson = Gson()
    val file = File("./registroAlumno.json")
    val jsonRegistroNoActualizado = file.readText()
    val registroAlumno = gson.fromJson(jsonRegistroNoActualizado, RegistroAlumno::class.java)
    registroAlumno.anyadirCamposProblema(problemaNuevo.id, problemaNuevo.numeroIntentos.toString(), problemaNuevo.resuelto.toString(), problemaNuevo.listaIntentos)
    val jsonRegistroActualizado = gson.toJson(registroAlumno)
    file.writeText("")
    file.writeText(jsonRegistroActualizado)

    println("Lista de problemas actualizada.")
  }

  /**
   * Nos permite crear un informe de notas del alumno en función de los problemas resueltos
   * Tenemos en cuenta también el número de intentos para cada problema resuelto.
   */
  private fun generarInformeAlumno() {
    //Fórmula de puntuación individual de problemas resueltos:
        //SI numIntentos <= 5 NOTA_P=10-(1*numIntentos) SINO NOTA = 0
    //Fórmula ponderada para cálculo total de la nota
        // ponderacion <- 1/numProblemasResueltos
        // NOTA FINAL = sumatorio(NOTA_P1*ponderacion...NOTA_PN*ponderacion)
    val informeAlumno = InformeAlumno()
    UI.mostrarInformeAlumno(informeAlumno)
  }

  /**
   * Sirve para comparar la contraseña introducida por el usuario y la contraseña del sistema que tiene asignanda el profesor.
   * @return Boolean - Nos indica si las contraseñas son iguales o no.
   */
  private fun comprobarContrasenya(): Boolean{
    println("ERROR -> Por favor, introduce la contraseña para acceder al rol de profesor:")
    val contrasenyaIntroducida = readln().trim().lowercase()
    return contrasenyaIntroducida == this.contrasenya
  }
}