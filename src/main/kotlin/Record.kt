import com.google.gson.Gson
import java.io.File

/**
 * Clase que encapsula la serialización de un objeto 'Problema' .
 * @author Carlos Tarrias Diaz
 * @version 1.0.0
 * @see Problema
 */
class Record(ruta:String) {
    var rutaFichero = ruta

  /**
   * Sirve para deserializar un problema en la carpeta 'Problemas'.
   */
  fun getProblemaDeJSON(): Problema {
      val gson = Gson()
      val json = File(this.rutaFichero).readText()
      return gson.fromJson(json, Problema::class.java)
    }
}