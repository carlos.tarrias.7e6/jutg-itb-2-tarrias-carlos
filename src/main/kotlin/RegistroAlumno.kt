/**
 * Representa los *datos del problema* que tiene almacenado el alumno en el sistema.
 * *Es decir, un detalle simplificado de cada problema.
 * Tipo -> data : sólo sirve para almacenar datos que se van a serializar.
 * Indicamos @Transient para que a la hora de serializar un problema, este campo se ignore.
 * @author Carlos Tarrias Diaz
 * @version 1.0.0
 * @see Alumno
 * @see ListaProblemas
 */
data class RegistroAlumno(@Transient val nom: String) {

    //Inicialización de atributos.
    var problemas = mutableListOf<Campos>()

    //Getters
    fun getId(id: Int): String {
        return this.problemas[id].id
    }

    fun getNumIntentos(id: Int): String {
        return this.problemas[id].numIntentos
    }

    fun getResuelto(id: Int): String {
        return this.problemas[id].resuelto
    }

    fun getListaIntentos(id: Int): MutableList<String> {
        return this.problemas[id].listaIntentos
    }

  /**
   * Sirve para añadir un nuevo detalle del problema al registro del alumno.
   * @param id String : Id del problema.
   * @param numIntentos String : Número de intentos del problema.
   * @param resuelto String : Indica si el problema está resuelto o no.
   * @param listaIntentos String : Lista de respuestas anteriores al problema.
   */
  fun anyadirCamposProblema(id:String, numIntentos:String, resuelto:String, listaIntentos:MutableList<String>) {
      val campos = Campos("")
      campos.id = id
      campos.numIntentos = numIntentos
      campos.resuelto = resuelto
      campos.listaIntentos = listaIntentos
      problemas.add(campos)
  }

  /**
   * Sirve para actualizar las campos de un problema de la lista.
   * @param id String : Id del problema.
   * @param numIntentos String : Número de intentos del problema.
   * @param resuelto String : Indica si el problema está resuelto o no.
   * @param listaIntentos String : Lista de respuestas anteriores al problema.
   * @param indice Int : Índice del problema en la lista.
   */
  fun cambiarAtributosProblema(id:String, numIntentos:String, resuelto:String, listaIntentos:MutableList<String>, indice:Int) {
    this.problemas[indice].id = id
    this.problemas[indice].numIntentos = numIntentos
    this.problemas[indice].resuelto = resuelto
    this.problemas[indice].listaIntentos = listaIntentos
  }

     // Clase interna que contiene los datos a serializar. Sería la versión simple de un problema que
     // queremos hacer persistente en el sistema para el alumno.
     class Campos(var nom: String) {
        var id : String = ""
        var numIntentos : String = ""
        var resuelto : String = ""
        var listaIntentos : MutableList<String> = mutableListOf()
    }

}