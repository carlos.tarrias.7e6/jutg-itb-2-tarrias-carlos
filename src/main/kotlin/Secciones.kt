/**
 * Representa los diferentes tipos de los problemas del juez.
 * @author Carlos Tarrias Diaz
 * @version 1.0.0
 * @see Problema
 */
enum class Secciones {
  DEFAULT,
  TDADES,
  CONDICIONAL,
  BUCLE,
  LLISTA
}