import java.io.File
import kotlin.system.exitProcess
/**
 * Representa el sistema que permite iniciar la aplicación en modo alumno o en modo profesor.
 * Tipo -> estática : sólo nos interesa tener una instancia principal del sistema a lo largo de la ejecución.
 * @author Carlos Tarrias Diaz
 * @version 1.0.0
 * @see UI
 * @see IUsuario
 * @see Alumno
 * @see Profesor
 */
class SistemaJutgeITB {

  //Para permitir que la clase sea estática tenemos que crear un 'companion object'.
  companion object {

    //Inicialización de atributos.
    private var usuario : IUsuario = Alumno("")
    private var salirMenu : Boolean = false
    private var opcionUsuario = ""

    //El número de problemas que contiene el sistema lo hacemos persistenete en un archivo de texto para simplificar y facilitar
    //La carga y guardado de datos del alumno y de los problemas que inserte el profesor.
    private var numProblemes = File("./numProblemas.txt").readText().toInt() //Suponemos que siempre existe este fichero.

    /**
     * Incrementa el número del problema del sistema y sobreescribe el archivo 'numProblemas.txt'
     * para evitar inconsistencia de datos.
     */
    fun incrementarNumeroProblemas() {
      this.numProblemes++
      val fileNumProblemas = File("./numProblemas.txt")
      fileNumProblemas.writeText("") //Sobreescribimos el archivo.
      fileNumProblemas.writeText("${this.numProblemes}")
    }

    /**
     * Permite obtener el número de problemas del sistema.
     */
    fun getNumProblemes(): Int =  this.numProblemes

    /**
     * Pone en marcha el sistema del juez ITB y muestra el menú concreta en función del tipo de usuario.
     */
    fun iniciar() {
      UI.mostrarMensajeBienvenida()
      this.usuario = iniciarUsuario()
      do {
        UI.mostrarMenuInicial(this.usuario)
        this.opcionUsuario = readln().trim()
        this.salirMenu = this.usuario.activarFuncionalidad(this.opcionUsuario)
      }while (!this.salirMenu) //Hasta que el usuario no lo indique vamos mostrando el menú correspondiente.
      println("¡Saliendo del juez ITB, ¡hasta pronto!")
    }

    /**
     * Permite instanciar un alumno o un profesor en función de la opción que introduzca el usuario.
     * @return IUsuario : Usuario de tipo alumno o de tipo profesor.
     */
    private fun iniciarUsuario(): IUsuario {
      val usuario: IUsuario = when (readln().trim().uppercase()) {
        "A" -> Alumno("Carlos")
        "P" -> Profesor("Profe")
        else  -> exitProcess(-1) //Para simplificar, salimos del programa si el usuario no introduce la opción correcta.
      }
      return usuario
    }

  }

}