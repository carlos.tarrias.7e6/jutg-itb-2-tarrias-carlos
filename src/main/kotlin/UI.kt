/**
 * Clase para poder mostrar diferentes tipos de mensaje por la terminal.
 * Tipo -> estática : sólo nos interesa tener una instancia principal del sistema a lo largo de la ejecución.
 * @see SistemaJutgeITB
 */
class UI {

    //Para permitir que la clase sea estática tenemos que crear un 'companion object'.
  companion object {

    /**
     * Muestra el mensaje de bienvenida al iniciar el programa y pide introducir el tipo de usuario.
     */
    fun mostrarMensajeBienvenida() {
      val logo = """    
      |       __   __    __  .___________.  _______  __  .___________..______   
      |      |  | |  |  |  | |           | /  _____||  | |           ||   _  \  
      |      |  | |  |  |  | `---|  |----`|  |  __  |  | `---|  |----`|  |_)  | 
      |.--.  |  | |  |  |  |     |  |     |  | |_ | |  |     |  |     |   _  <  
      ||  `--'  | |  `--'  |     |  |     |  |__| | |  |     |  |     |  |_)  | 
      | \______/   \______/      |__|      \______| |__|     |__|     |______/  
      | 
      |Eres un alumno o un profesor?
      |Introduce A para alumno ó introduce P para profesor
      """.trimMargin()
      println(logo)
    }

    /**
     * Muestra el mensaje de bienvenida al iniciar el programa y pide introducir el tipo de usuario.
     * @param usuario : IUsuario - Usuario que ha instanciado el sistema.
     */
    fun mostrarMenuInicial(usuario:IUsuario) {
      if (usuario is Alumno) mostrarMenuAlumno(usuario)
      else if (usuario is Profesor) mostrarMenuProfesor(usuario)
    }

    /**
     * Muestra el informe de notas del alumno.
     * Se muestra:
     * El número de problemas resueltos.
     * Para cada problema resuelto, la nota correspondiente.
     * La nota final respecto a los problemas resueltos.
     * @param informe : InformeAlumno - Informe del alumno creado por el sistema.
     */
    fun mostrarInformeAlumno(informe: InformeAlumno) {
      println("===================INFORME DEL ALUMNO======================")
      println("PROBLEMAS RESUELTOS : ${informe.numProblemasResueltos}")
      for (p in informe.problemasResueltos) {
        println("NOTA PROBLEMA ${p.id} -> ${if (p.numIntentos.toInt()-1 <= 5) 10 - (1*p.numIntentos.toInt()-1) else 0}")
      }
      println("NOTA FINAL -> ${informe.notaFinal} ")

    }

    /**
     * Muestra la ayuda del sistema en el modo alumno.
     */
      fun mostrarAyuda() {
          val ayuda = """
              |=================================================================AYUDA JUEZ ITB========================================================
              |
              | >>>>> El siguiente texto hace referencia al funcionamiento de las diferentes opciones del menú principal.
              | >>>>> OPCIÓN 1: Muestra la lista de problemas resueltos y hace enfásis en el primer problema no resuelto para dar opción a resolverlo
              | >>>>> Una vez mostrados todos los problemas, puedes volver a ver la lista si así lo deseas.
              |
              | >>>>> OPCIÓN 2: Muestra una lista de los problemas del sistema (indicando si están resueltos o no) y por cada intento muestra una cruz (X).
              | >>>>> Después debes indicar el número del problema a resolver (el número debe existir en la lista de problemas).
              |
              | >>>>> OPCIÓN 3: Muestra una lista de los problemas del sistema (indicando si están resueltos o no) y por cada intento muestra una cruz (X).
              """.trimIndent()
          println(ayuda)
          println("")
      }

    /**
     * Muestra el menú de alumno.
     * @param usuario : IUsuario - Usuario que ha instanciado el sistema.
     */
      private fun mostrarMenuAlumno(usuario: IUsuario) {
      val menuAlumno = """
      |BIENVENIDO Alumno [${usuario.nombre}]
      |1.Seguir con el itinerario de aprendizaje
      |2.Lista de problemas 
      |3.Consultar histórico de problemas resueltos
      |4.Ayuda
      |5.Salir
      |Introduce la opción por favor:
      """.trimMargin()
      print(menuAlumno)
    }

    /**
     * Muestra el menú de profesor.
     * @param usuario : IUsuario - Usuario que ha instanciado el sistema.
     */
   private fun mostrarMenuProfesor(usuario: IUsuario){
      val menuAlumno = """
      |BIENVENIDO Profesor [${usuario.nombre}]
      |1.Añadir nuevo problema
      |2.Generar informe alumno
      |3.Salir
      |Introduce la opción por favor:
      """.trimMargin()
      print(menuAlumno)
    }
  }
}