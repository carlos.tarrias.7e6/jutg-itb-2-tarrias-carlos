///**
// * Se encarga de añadir los diferentes problemas (por medio de archivos de texto) a la lista de problemas del juez.
// * @param listaProblemas MutableList<Problema> : Lista que contendrá los diferentes problemas.
// */
//fun cargarProblemas(listaProblemas: MutableList<Problema>) {
//  val numProblemas = 5
//
//  for (i in 1..numProblemas) {
//    val p = Problema("./Problemas/p${i}.txt", "./CasosPrueba/cp${i}.txt")
//    listaProblemas.add(p)
//  }
//} YA NO SE USA
//
///**
// * Muestra por la terminal los enunciados de los diferentes problemas en bucle hasta que el usuario decida terminar de verlos.
// * De cada problema se muestra los intentos realizados. Si no se ha resuelto indicamos al usuario si quiere resolverlo, y si ya lo está, pasamos al siguiente problema.
// * @param listaProblemas MutableList<Problema> : Lista que contiene los diferentes problemas.
// */
//fun mostrarProblemas(listaProblemas: MutableList<Problema>) {
//  var opcionUsuarioMenu : String
//  do {
//    for (problema in listaProblemas) {
//      //Si el problema ya está resuelto o hemos indicado que no queremos resolverlo, pasamos al siguiente problema de la lista.
//      if (!decidirProblema(problema)) continue
//      //Mostramos enunciados del problema y sus casos públicos.
//      println(problema)
//      problema.mostraCasosPublicos()
//      resolverProblema(problema)
//    }
//    println("¿Quieres ver los enunciados otra vez? (Sí:S/s , No:N/n)")
//    opcionUsuarioMenu = readln().trim()
//  }while (opcionUsuarioMenu !in "nN") //Mostramos los enunciados en bucle hasta que el usuario indique lo contrario al final.
//} YA NO SE USA

/**
 * Muestra por terminal la opción de resolver el problema (si no está resuelto). Si está resuelto, se le informa al usuario.
 * @param problema Problemas : Problema actual que se está mostrando en la terminal.
 * @return Boolean : Representa si el usuario quiere resolver el problema (true) o no (false).
 */
fun decidirProblema(problema: Problema): Boolean {
  if (problema.resuelto) {
    println("El problema ya se ha resuelto. Pasando al siguiente problema")
    return false
  }
  println("¿Quieres resolver este problema? (Sí:(S/s) No:(N/n))")
  var opcion: String = readln().trim()
  while(opcion !in "SsnN") { //Vamos pidiendo input del usuario hasta que esté en el formato correcto.
    println("La Opción introducida no es válida. Vuélvelo a intentar, por favor:")
    opcion = readln().trim()
  }
  if (opcion in "Nn") return false
  return true
}

/**
 * Muestra por terminal el caso privado del problema actual y le pide al usuario su respuesta.
 * @param problema Problemas : Problema actual que se está mostrando en la terminal.
 */
fun resolverProblema(problema: Problema) {
  var seguirIntentando = ""
  var resuelto = false
  do {
    problema.mostraCasosPrivados() //Mostramos el caso privado del problema.
    println("Introduce tu propuesta de solución por favor (sólo un único valor):")
    val respuestaUsuario = readln().trim()
    problema.addIntento(respuestaUsuario) //Añadimos la respuesta a la lista de respuestas introducidas por el usuario.
    if (respuestaUsuario == problema.getRespuesta()) { //Si acertamos la respuesta, indicamos que el problema está resuelto.
      println("¡Enhorabuena, Has resuelto el problema!")
      problema.resuelto = true
      resuelto = true
    }
    else { //Si no acertamos, se indica si queremos otro intento.
      println("La solución propuesta no es correcta..lástima")
      println("¿Quieres seguir intentándolo? (Sí(S/s) - No(N/n)")
      seguirIntentando = readln().trim()
    }
    problema.incrementarIntentos() //Incrementamos el numero de intentos del problema.

  }while (!resuelto && (seguirIntentando in "Ss")) //Vamos dando oportunidades de resolver el problema si el usuario así lo indica.

}