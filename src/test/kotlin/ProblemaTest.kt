import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*

internal class ProblemaTest {

  private val problemaTest = Problema("./Problemas/p1.txt", "./CasosPrueba/cp1.txt")

  //fun addIntento() Tests
  @Test
  fun compruebaListaIntentosNoEstaVaciaAlInsertar() {
    problemaTest.addIntento("8")
    assertTrue(problemaTest.listaIntentos.isNotEmpty())
  }

  @Test
  fun compruebaListaIntentosTieneRespuestasValidas() {
    problemaTest.addIntento("7")
    problemaTest.addIntento("6")
    problemaTest.addIntento("5")
    problemaTest.addIntento("8")
    assertTrue(problemaTest.listaIntentos.all { it.isNotEmpty() })
  }

  // fun incrementarIntentos() Tests
  @Test
  fun compruebaIntentosNuncaEsNegativoYMayorACeroAlIncrementar() {
    problemaTest.incrementarIntentos()
    assertTrue(problemaTest.numeroIntentos > 0)
  }

  //fun getRespuesta() Tests
  @Test
  fun compruebaQueRespuestaNoEsVacia() {
    assertTrue(problemaTest.getRespuesta().isNotEmpty())
  }
}